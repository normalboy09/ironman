# We need to import request to access the details of the POST request
# and render_template, to render our templates (form and response)
# we'll use url_for to get some URLs for the app on the templates
from flask import Flask, render_template, request, url_for
from pymongo import MongoClient

# Initialize the Flask application
app = Flask(__name__)
client = MongoClient('127.0.0.1:27017')
db = client.iron_man


# Define a route for the default URL, which loads the form
@app.route('/',methods=['POST','GET'])
def form():
    #auto Update name with Role name
    autoUpdateRoleName()
    ########
    deviceCol1=[]
    deviceCol_id=[x for x in db.device.find()]
    max=0
    n=len(deviceCol_id)
    for i in range(n):
        deviceIdTemp=deviceCol_id[i]['device_id']
        if deviceIdTemp>0:
            if deviceIdTemp>max:
                max=deviceIdTemp
    err=''
    if request.method == 'POST':
            custormer_id=request.form['customer_id']
            name = request.form['name']
            vendor = request.form['vendor']
            hardware = request.form['hardware']
            ios = request.form['ios']
            ip_mgmt = request.form['ip_mgmt']
            port_mgmt = request.form['port_mgmt']
            method_connect = request.form['method']
            username = request.form['username']
            password = request.form['password']
            location = request.form['location']
            email = request.form['email']
            mobile = request.form['mobile']
            device_id=max+1

            device_duplicate=[x for x in db.device.find({"ip_mgmt":ip_mgmt})]
            if len(device_duplicate) >0 or ip_mgmt=='':
                err='Duplicate IP!!!Please input again!'
            else:
                role_find=checkRole(name)
                db.device.insert({
                    "device_id":device_id,
                    "customer_id":custormer_id,
                    "name": name,
                    "vendor": vendor,
                    "hardware": hardware,
                    "ios": ios,
                    "role": role_find,
                    "ip_mgmt": ip_mgmt,
                    "port_mgmt": port_mgmt,
                    "method":method_connect,
                    "username": username,
                    "password": password,
                    "location": location,
                    "email": email,
                    "mobile": mobile
                })
                err='Successful'
            deviceCol1=[x for x in db.device.find({})]
    else:
          deviceCol1=[x for x in db.device.find({})]
    return render_template('form_submit.html',dev1=deviceCol1,err=err)


# Define a route for the action of the form, for example '/hello/'
# We are also defining which type of requests this route is
# accepting: POST requests in this case
@app.route('/editNode/<int:id>',methods=['GET','POST'])
def editNode(id):
    err=''
    id_device=id
    device_edit=[x for x in db.device.find({"device_id":id_device})]
    device_all=[x for x in db.device.find()]
    if request.method == 'POST':
            custormer_id=request.form['customer_id']
            name = request.form['name']
            vendor = request.form['vendor']
            hardware = request.form['hardware']
            ios = request.form['ios']
            role = checkRole(name)
            ip_mgmt = request.form['ip_mgmt']
            port_mgmt = request.form['port_mgmt']
            method_connect = request.form['method']
            username = request.form['username']
            password = request.form['password']
            location = request.form['location']
            email = request.form['email']
            mobile = request.form['mobile']
            device_id=id
            device_duplicate=[x for x in db.device.find({"ip_mgmt":ip_mgmt})]
            if  ip_mgmt=='':
                err='IP can not be null  !!!  Please input again!'
            else:
                db.device.update(
                    {"device_id":device_id },
                    {'$set':  {"device_id":device_id,
                    "customer_id":custormer_id,
                    "name": name,
                    "vendor": vendor,
                    "hardware": hardware,
                    "ios": ios,
                    "role": role,
                    "ip_mgmt": ip_mgmt,
                    "port_mgmt": port_mgmt,
                    "method":method_connect,
                    "username": username,
                    "password": password,
                    "location": location,
                    "email": email,
                    "mobile": mobile
                }}
                )
                err='Edit Successful'
            device_all=[x for x in db.device.find()]
            device_edit=[x for x in db.device.find({"device_id":id_device})]
    return render_template('form_edit.html',dev_edit=device_edit,dev_all=device_all,err=err)
@app.route('/deleteNode/<int:id>',methods=['GET','POST'])
def deleteNode(id):
    err=''
    id_device=id
    device_edit=[x for x in db.device.find({"device_id":id_device})]
    device_all=[x for x in db.device.find()]
    if request.method == 'POST':
            device_id=id
            device_duplicate=[x for x in db.device.find({"device_id":device_id})]

            db.device.remove(
                    {"device_id":device_id }                )
            err='Delete Successful'
            device_all=[x for x in db.device.find()]
            device_edit=[x for x in db.device.find({"device_id":id_device})]
    return render_template('form_delete.html',dev_edit=device_edit,dev_all=device_all,err=err)

@app.route('/role/',methods=['POST','GET'])
def role():
    roleCol1=[]
    roleCol_id=[x for x in db.role.find()]
    max=0
    n=len(roleCol_id)
    for i in range(n):
        roleIdTemp=roleCol_id[i]['role_id']
        if roleIdTemp>0:
            if roleIdTemp>max:
                max=roleIdTemp
    err=''
    if request.method == 'POST':
            role_name = request.form['role_name']
            device_name = request.form['device_name']
            role_id=max+1
            role_duplicate=[x for x in db.role.find({"role_name":role_name})]
            device_name_duplicate=[x for x in db.role.find({"device_name":device_name})]
            if len(role_duplicate) >0 or role_name=='' or len(device_name_duplicate)>0 or device_name=='' :
                err='Duplicate Role Name or Device Name or blank Device Name , Role Name!!!Please input again!'
            else:
                db.role.insert({
                    "role_id":role_id,
                    "device_name":device_name,
                    "role_name": role_name
                })
                err='Successful'
            roleCol1=[x for x in db.role.find({})]
    else:
          roleCol1=[x for x in db.role.find({})]
    return render_template('form_role.html',role1=roleCol1,err=err)

@app.route('/deleteRole/<int:id>',methods=['GET','POST'])
def deleteRole(id):
    err=''
    id_role=id
    role_edit=[x for x in db.role.find({"role_id":id_role})]
    role_all=[x for x in db.role.find()]
    if request.method == 'POST':
            role_id=id
            db.role.remove(
                    {"role_id":role_id })
            err='Delete Successful'
            role_all=[x for x in db.role.find()]
    return render_template('form_delete_role.html',role_all=role_all,err=err)
@app.route('/editRole/<int:id>',methods=['GET','POST'])
def editRole(id):
    err=''
    id_role=id
    role_edit=[x for x in db.role.find({"role_id":id_role})]
    role_all=[x for x in db.role.find()]
    if request.method == 'POST':
            role_name = request.form['role_name']
            device_name = request.form['device_name']
            role_id=id
            role_duplicate=[x for x in db.role.find({"role_name":role_name})]
            if  role_name=='':
                err='Role Name can not be null  !!!  Please input again!'
            else:
                db.role.update(
                    {"role_id":role_id },
                    {'$set':  {
                    "role_name": role_name,
                    "device_name": device_name,

                }}
                )
                err='Edit Successful'
            role_all=[x for x in db.role.find()]
            role_edit=[x for x in db.role.find({"role_id":id_role})]
    return render_template('form_edit_role.html',role_edit=role_edit,role_all=role_all,err=err)

def checkRole(deviceName):
    #check Role type for this hostname
        role_list=[x for x in db.role.find()]
        role_find=''
        sum_role=len(role_list)
        for i in role_list:
            device_name_contain=i['device_name']
            role_name=i['role_name']
            if deviceName.find(device_name_contain) >=0:
                role_find=role_name
        if role_find=='':
            role_find='Unspecified'
        return role_find
def autoUpdateRoleName():
    deviceCol=[x for x in db.device.find()]
    for n in deviceCol:
        name=n['name']
        id=n['device_id']
        role=n["role"]
        role_check=checkRole(name)
        if role_check != role:
            db.device.update(
                    {"device_id":id },
                    {'$set':  {
                   "role":role_check

                }}
                )

if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=int("8080")
    )
