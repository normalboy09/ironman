from wtforms import Form, TextField, validators
from flask import render_template ,redirect,url_for,request,flash,Flask
from pymongo import MongoClient
class CreateAuthorForm(Form):
    names = TextField('Names', [validators.Length(min=5, max=70)])

app=Flask(__name__)
class AuthorForm(Form):
    name = TextField('Name')

@app.route('/author/create', methods = ['GET', 'POST'])
def create_author():
    form = CreateAuthorForm(request.form)
    if request.method == 'POST' and form.validate():
        author = AuthorForm(form.names.data)
        client= MongoClient('localhost:27017')
        db=client.device
        db.session.add(author)
        db.session.commit()
        flash('Author is .')

        return redirect(url_for('main.display_authors'))

    return render_template('form.html', form=form)

if __name__ == '__main__':
  app.run(
        host="127.0.0.1",
        port=int("8080")
  )
